const input = document.getElementById("myForm");
input.addEventListener("focus", myFocusFunction, true);
input.addEventListener("blur", myBlurFunction, true);

function myFocusFunction() {
    document.getElementById("myInput").style.borderColor = "green";

}

function myBlurFunction() {
    document.getElementById("myInput").style.borderColor = "";
    document.getElementById('price').innerHTML = 'Текущая цена: $ ' + document.getElementById('myInput').value + '<button><img src="cross.png"></button>';
    document.getElementById("myInput").style.backgroundColor = "green";
}

let pane = document.querySelectorAll('.pane');

pane.forEach(block => block.addEventListener('click', removeBlock));

function removeBlock() {
    let block = this;
    block.style.opacity = 1;
    let blockId = setInterval(function () {
        if (document.getElementById("myInput").validity.rangeUnderflow) {
            document.getElementById("myInput").style.borderColor = "red";
            document.getElementById("myInput").style.backgroundColor = "";
        }
        document.getElementById("demo").innerText = `Please enter correct price`;
        document.getElementById("myInput").style.backgroundColor = "";
        if (block.style.opacity > 0) block.style.opacity -= .1;
        else {
            clearInterval(blockId);
            block.remove();
        }

    }, 1);
}